## Conway's Game of Life

A simple, naive implementation of Conway's Game of Life using a HTML table.

This is forked from https://github.com/JoshuaLeeGMIT/game-of-life, my
first-year web development project with features I consider superfluous
stripped out.
